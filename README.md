# Tienda Geli

## Descripción

Aplicación web para la gestión de clientes, ventas, inventarios, productos, pedidos entre otros, para una tienda virtual que ofrece productos para el cuidado del cabello y la piel en mujeres y hombres.

Esta aplicación se esta realizando para la entrega de un proyecto en el programa de Misión TIC 2022 y solo es para fines educativos.

## Logo

[![Logo Tienda Geli](https://i.imgur.com/gIxZ6vv.png "Logo Tienda Geli")](https://i.imgur.com/gIxZ6vv.png "Logo Tienda Geli")

## Preview de la aplicación (Login)
[![Preview Tienda Geli](https://i.imgur.com/GLsJ1rH.png "Preview Tienda Geli")](https://i.imgur.com/GLsJ1rH.png "Preview Tienda Geli")


## Autores

Ingrid Marcela Vargas - **Desarrollador**
Hugo Andres Marrugo Polo - **Scrum Master** - **Desarrollador**
Elkin Pardo - **Desarrollador**
Andres Camilo Bohorquez Bogota - **Desarrollador**
John Bayron Castañeda Zuleta - **Product Owner** - **Desarrollador**