from django.db import models
from geliApp.models.productos import Productos
from geliApp.models.proveedores import Proveedores

class Compras(models.Model):
    id_producto = models.ForeignKey(Productos, related_name='producto', on_delete=models.CASCADE, blank=False, null=False)
    id_proveedor = models.ForeignKey(Proveedores, related_name='proveedor', on_delete=models.CASCADE, blank=False, null=False)
    fecha_compra = models.DateField(auto_now=False, auto_now_add=True)
    cantidad = models.IntegerField(blank=False, null=False)
    precio = models.IntegerField(blank=False, null=False)
    total = models.IntegerField(blank=False, null=False)
    numero_factura = models.IntegerField(blank=False, null=False)
    
    class Meta:
        verbose_name = 'Compra'
        verbose_name_plural = 'Compras'