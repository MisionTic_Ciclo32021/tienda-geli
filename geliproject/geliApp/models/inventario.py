from django.db import models
from geliApp.models.productos import Productos

class Inventario(models.Model):
    id_producto = models.ForeignKey(Productos, related_name='existencia', on_delete=models.CASCADE)
    cantidad = models.IntegerField()
    
    class Meta:
        verbose_name = 'Existencia'
        verbose_name_plural = 'Existencias'