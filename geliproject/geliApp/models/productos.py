from django.db import models

class Productos(models.Model):
    nombre_producto = models.CharField('Nombre de Producto', max_length=100, blank=False, null=False, unique=True)
    descripcion = models.CharField('Descripción', max_length=255, blank=True, null=True, unique=False)
    categoria = models.CharField('Categoria', max_length=100, blank=False, null=False, unique=False)
    marca = models.CharField('Marca', max_length=100, blank=False, null=False, unique=False)
    
    
    class Meta:
        verbose_name = 'Producto'
        verbose_name_plural = 'Productos'