from django.db import models

class Proveedores(models.Model):
    nit = models.CharField('NIT', max_length=20, blank=False, null=False, unique=True)
    razon_social = models.CharField('Razón Social', max_length=200, blank=False, null=False, unique=True)
    telefono = models.BigIntegerField('Telefono')
    correo = models.EmailField('Correo Electronico', max_length=254)
    
    class Meta:
        verbose_name = 'Proveedor'
        verbose_name_plural = 'Proveedores'