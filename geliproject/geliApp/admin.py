from django.contrib import admin
from .models.inventario import Inventario
from .models.productos import Productos
from .models.proveedores import Proveedores
from .models.compras import Compras
from .models.user import User


# Register your models here.
#admin.site.register(bienvenido)
admin.site.register(Compras)
admin.site.register(Inventario)
admin.site.register(Productos)
admin.site.register(Proveedores)
admin.site.register(User)