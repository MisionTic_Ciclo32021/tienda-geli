#Importar librerias de django para hacer el CRUD
from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view
#Importar el modelo de inventario y el serializador de inventario creados previamente

#LIBRERIAS PRODUCTOS
from geliApp.models.productos import Productos
from geliApp.serializers.productosSerializer import ProductosSerializer
from geliApp.models.inventario import Inventario
from geliApp.serializers.inventarioSerializer import InventarioSerializer
from geliApp.models.compras import Compras
from geliApp.serializers.comprasSerializer import ComprasSerializer


#LIBRERIAS INVENTARIO
from geliApp.models.inventario import Inventario
from geliApp.serializers.inventarioSerializer import InventarioSerializer

#LIBERIAS COMPRAS
from geliApp.models.compras import Compras
from geliApp.serializers.comprasSerializer import ComprasSerializer

#LIBRERIAS PROVEEDORES
from geliApp.models.proveedores import Proveedores
from geliApp.serializers.proveedoresSerializer import ProveedoresSerializer

"""
                                            CRUD PRODUCTOS
"""


@api_view(['GET', 'POST'])
def productos_api_view(request):
    
    # READ
    if request.method == 'GET':
        productos = Productos.objects.all().order_by('id')
        productos_serializer = ProductosSerializer(productos, many=True)
        return Response(productos_serializer.data, status=status.HTTP_200_OK)
    
    # CREATE
    elif request.method == 'POST':
        productos_serializer = ProductosSerializer(data=request.data)
        
        if productos_serializer.is_valid():
            productos_serializer.save()
            return Response(productos_serializer.data, status=status.HTTP_201_CREATED)
        
        return Response(productos_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
@api_view(['GET', 'PUT', 'DELETE'])
def productos_detail_api_view(request, pk=None):
    
    producto = Productos.objects.filter(id=pk).first()
    
    if producto:
        # READ
        if request.method == 'GET':
            producto_serializer = ProductosSerializer(producto)
            return Response(producto_serializer.data, status=status.HTTP_200_OK)
        
        # UPDATE
        elif request.method == 'PUT':
            producto_serializer = ProductosSerializer(producto, data=request.data)
            if producto_serializer.is_valid():
                producto_serializer.save()
                return Response(producto_serializer.data, status=status.HTTP_200_OK)
            return Response(producto_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        # DELETE
        elif request.method == 'DELETE':
            producto.delete()
            return Response("Producto eliminado exitosamente.", status=status.HTTP_200_OK)
    
    return Response("Producto no encontrado", status=status.HTTP_400_BAD_REQUEST)


"""
                                            CRUD PROVEEDORES
"""

    #Crud para El Modelo Proveedores
@api_view(['GET','POST'])
def proveedores_api_view(request):
#Read
    #verifica el metodo, si es Get(Por la Url y enter)
    if request.method=='GET':
        #se crea una variable que va a recibir todos los objetos de la tabla proveedores
        proveedores=Proveedores.objects.all().order_by('id')
        #la variable pasa a formato Json, manu dice que tra muchos arhivos y los guarda en la varible proveedores
        proveedores_serializer=ProveedoresSerializer(proveedores,many=True)
        return Response(proveedores_serializer.data,status=status.HTTP_200_OK) 
    
    
#create
    #Metodo Post, se usa para crear un proveedor
    elif request.method=='POST':
        #se trae la informacion que esta guardada
        proveedores_serializer = ProveedoresSerializer(data=request.data)
        #Se valida que esa informacion esta correcta
        #Los validadores por defecto de Django, verifican que los datos ingresados, coincida
        #con los tipos de datos guardados en la base de datos
       
        #si la validacion es correcta se registra en la base de datos       
        if proveedores_serializer.is_valid():
            proveedores_serializer.save()
            #se responde correctamente , sino sale error
            return Response(proveedores_serializer.data, status=status.HTTP_201_CREATED)
        
        return Response(proveedores_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

#Para la Editar y eliminar, se usa el get para traer la informacion del campo en especifico que se va a eliminar
#put elimina y el delete, borra
@api_view(['GET', 'PUT', 'DELETE'])

#se usa un filtro y se manda a llamar el proveedor por su Id
def proveedores_detail_api_view(request, pk=None):
    
    proveedores = Proveedores.objects.filter(id=pk).first()
    
    #si existen proveedores lo trae
    if proveedores:
        # READ
        if request.method == 'GET':
            proveedores_serializer = ProveedoresSerializer(proveedores)
            return Response(proveedores_serializer.data, status=status.HTTP_200_OK)
        
        # UPDATE
        elif request.method == 'PUT':
            proveedores_serializer = ProveedoresSerializer(proveedores, data=request.data)
            if proveedores_serializer.is_valid():
                proveedores_serializer.save()
                return Response(proveedores_serializer.data, status=status.HTTP_200_OK)
            return Response(proveedores_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        # DELETE
        elif request.method == 'DELETE':
            proveedores.delete()
            return Response("proveedor eliminado exitosamente.", status=status.HTTP_200_OK)
    
    return Response("proveedor no encontrado", status=status.HTTP_400_BAD_REQUEST)




"""
                                                CRUD IVENTARIO
"""

#Reescrbir el GET y POST de django rest framework
@api_view(['GET', 'POST'])
#Definir variable para leer, mostrar y crear objetos en la tabla inventarios, resiviendo la acción que elija el usuario
def inventario_api_view(request):
    """
    Función para leer tablas y crearlas
    """
    #Leer y mostrar la tabla inventario cuando el usuario use GET
    if request.method == 'GET':
        #Almacenar todos los datos de campos de las tablas de la base de datos en una variable
        inventario = Inventario.objects.all().order_by('id')
        #Pasar la variable por el serializador para transformarlos en json y almacenarlos en otra variable
        inventario_serializer = InventarioSerializer(inventario, many=True)
        #Retornar el json con los datos y decirle a la pagina que el resultado fue correcto
        return Response(inventario_serializer.data, status=status.HTTP_200_OK)

    # Almacenar datos en los campos de la tabla de inventario y crear un nuevo objeto
    elif request.method == 'POST':
        #Recibir los datos que ingrese el usuario (en json) y pasarlos por el serializador
        inventario_serializer = InventarioSerializer(data=request.data)

        #Continua con el codigo dentro del if si al pasar los datos por el serializador estos están en el formato correcto (json) y en general si cumplen con las restricciones del serliazador
        if inventario_serializer.is_valid():
            #La asegurarse de que los datos ingresados son validos, los guarda en la base de datos
            inventario_serializer.save()
            #Retornar el json con los nuevos datos y decirle a la pagina que los fueron creados nuevos datos en la base de datos
            return Response(inventario_serializer.data, status=status.HTTP_201_CREATED)

        #Retornar el error que genera no introducir correctamente los datos a la tabla y decirle a la pagina que ocurrió un error
        return Response(inventario_serializer.errors, status=status.HTTP_400_BAD_REQUEST)


#Reescrbir el GET, PUT Y DELETE de django rest framework
@api_view(['GET', 'PUT', 'DELETE'])
#Definir variable para leer, actualizar y borrar objetos en la tabla inventarios, resiviendo la acción que elija el usuario, junto a la pk (primary key) del objeto
def inventario_detail_api_view(request, pk=None):
    """
    Función para actualizar y borrar tablas
    """
    #Almacenar el primer dato de la base de datos que coincida con la pk especificada en una variable
    inventario = Inventario.objects.filter(id=pk).first()

    #Cuando el dato recuperado coincida con el pk especificado se avanzará con el codigo en el if
    if inventario:
        #Leer y mostrar la tabla que contenga el pk especificado por el usuario
        if request.method == 'GET':
            #Pasar la variable con el pk especificado por el serializador para transformarlos en json y almacenarlos en otra variable
            inventario_serializer = InventarioSerializer(inventario)
            #Retornar el json con los datos y decirle a la pagina que el resultado fue correcto
            return Response(inventario_serializer.data, status=status.HTTP_200_OK)

        #Actualizar la tabla que contenga el pk especificado por el usuario
        elif request.method == 'PUT':
            #Recibir los datos para actualización que ingresó el usuario (en json) y pasarlos por el serializador
            inventario_serializer = InventarioSerializer(inventario, data=request.data)
            #Verificar que los datos sean validos y si lo son, procede a actualizar la base de datos
            if inventario_serializer.is_valid():
                inventario_serializer.save()
                #Retornar el json con los datos y decirle a la pagina que el resultado fue correcto
                return Response(inventario_serializer.data, status=status.HTTP_200_OK)
                #Retornar el error que genera no introducir correctamente los datos a la tabla y decirle a la pagina que ocurrió un error
            return Response(inventario_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        #Borrar la tabla que contenga el pk especificado por el usuario
        elif request.method == 'DELETE':
            #Borrar la tabla que contiene el pk
            inventario.delete()
            #Retornar el json con los datos y decirle a la pagina que el resultado fue correcto
            return Response("Inventario eliminado exitosamente.", status=status.HTTP_200_OK )
        #Retornar el error que genera no introducir correctamente los datos a la tabla y decirle a la pagina que ocurrió un error
        return Response("Inventario no encontrado", status=status.HTTP_400_BAD_REQUEST)



# COMPRAS

"""
                                                    CRUD COMPRAS
"""

@api_view(['GET', 'POST'])
def compras_api_view(request):
    
    # READ
    if request.method == 'GET':
        compras = Compras.objects.all().order_by('id')
        compras_serializer = ComprasSerializer(compras, many=True)
        return Response(compras_serializer.data, status=status.HTTP_200_OK)
    
    # CREATE
    elif request.method == 'POST':
        compras_serializer = ComprasSerializer(data=request.data)
        
        if compras_serializer.is_valid():
            compras_serializer.save()
            return Response(compras_serializer.data, status=status.HTTP_201_CREATED)
        
        return Response(compras_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
@api_view(['GET', 'PUT', 'DELETE'])
def compras_detail_api_view(request, pk=None):
    
    compras = Compras.objects.filter(id=pk).first()
    
    if compras:
        # READ
        if request.method == 'GET':
            compras_serializer = ComprasSerializer(compras)
            return Response(compras_serializer.data, status=status.HTTP_200_OK)
        
        # UPDATE
        elif request.method == 'PUT':
            compras_serializer = ComprasSerializer(compras, data=request.data)
            if compras_serializer.is_valid():
                compras_serializer.save()
                return Response(compras_serializer.data, status=status.HTTP_200_OK)
            return Response(compras_serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        # DELETE
        elif request.method == 'DELETE':
            compras.delete()
            return Response("Compra eliminada exitosamente.", status=status.HTTP_200_OK)
    
    return Response("Compra no encontrada", status=status.HTTP_400_BAD_REQUEST)
