from django.http import response
from django.http.response import HttpResponse
from django.urls import path

from geliApp.api.api import productos_api_view, productos_detail_api_view, inventario_api_view, inventario_detail_api_view
from geliApp.api.api import productos_api_view, productos_detail_api_view
from geliApp.api.api import compras_api_view, compras_detail_api_view
from geliApp.api.api import proveedores_api_view, proveedores_detail_api_view
urlpatterns = [
    path('productos/', productos_api_view),
    path('productos/<int:pk>', productos_detail_api_view),
    path('inventario/', inventario_api_view),
    path('inventario/<int:pk>', inventario_detail_api_view),
    path('compras/', compras_api_view),
    path('compras/<int:pk>', compras_detail_api_view),
    path('proveedores/',proveedores_api_view),
    path('proveedores/<int:pk>',proveedores_detail_api_view)
]