from rest_framework import serializers
from geliApp.models.proveedores import Proveedores


class ProveedoresSerializer(serializers.ModelSerializer):
    class Meta:
        model = Proveedores
        fields = '__all__'