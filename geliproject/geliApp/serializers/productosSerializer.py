from rest_framework import serializers
from geliApp.models.productos import Productos


class ProductosSerializer(serializers.ModelSerializer):
    class Meta:
        model = Productos
        fields = '__all__'