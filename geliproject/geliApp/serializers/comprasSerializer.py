from rest_framework import serializers
from geliApp.models.compras import Compras


class ComprasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Compras
        fields = '__all__'