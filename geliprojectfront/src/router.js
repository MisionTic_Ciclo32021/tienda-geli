import { createRouter, createWebHistory } from "vue-router";
import App from "@/App.vue";

import LogIn from "@/components/LogIn.vue";
import Menu from "@/components/Menu";
import Proveedores from "@/views/Proveedores";
import Productos from "@/views/Productos";
import Inventario from "@/views/Inventario";
import Compras from "@/views/Compras";

const routes = [
  {
    path: "/",
    name: "root",
    component: App,
  },
  {
    path: "/logIn",
    name: "logIn",
    component: LogIn,
  },
  {
    path: "/menu",
    name: "menu",
    component: Menu,
  },
  {
    path: "/proveedores",
    name: "proveedores",
    component: Proveedores,
  },
  {
    path: "/productos",
    name: "productos",
    component: Productos,
  },
  {
    path: "/inventario",
    name: "inventario",
    component: Inventario,
  },
  {
    path: "/compras",
    name: "compras",
    component: Compras,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
