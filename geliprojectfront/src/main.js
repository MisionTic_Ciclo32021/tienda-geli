import { createApp } from "vue";
import VueNumerals from "vue-numerals";
import App from "./App.vue";
import router from "./router";

const app = createApp(App);

app.use(VueNumerals); // default locale is 'en'

// with options
app.use(VueNumerals, {
  locale: "fr",
});

app.use(router);

app.mount("#app");
